// const SELECTOR = {
//     MENU_FOOTER: ".menu-footer",
//     MENU_HEADER: ".menu-item",

//     MENU_FOOTER_TEXT: ".menu-text-footer",
//     MENU_HEADER_TEXT: ".menu-link",

//     // travese block
//     BLOCK: "div.sec",
//     BLOCK_DARK: "dark",
//     BLOCK_LIGHT: "light",

//     // text color
//     DARK_TEXT: "dark-text",
//     LIGHT_TEXT: "light-text",
// };
function setsizeFlex() {
    $('img.setSizeImg').each(function () {
        var $this = $(this);
        var $imgWrap = $this.parents('.article-img-wrap');
        var parentW = $imgWrap.outerWidth();
        var parentH = $imgWrap.outerHeight();
        var imgW = $this.width();
        var imgH = $this.height();
        // console.log('parentW:' + parentW + ', parentH:' + parentH + ', imgW:' + imgW + ', imgH:' + imgH);
        $this.css({ 'overflow': 'hidden', 'display': 'block' });
        $this.parent().css({ 'overflow': 'hidden', 'display': 'block' });
        var imgRatio = imgW / imgH;
        var parentRatio = parentW / parentH;

        if (imgRatio < parentRatio) {
            $this.css({
                'width': parentW + "px",
                'height': 'auto'
            });
        } else {
            $this.css({
                'width': 'auto',
                'height': parentH + "px"
            });
        };
    });
}
$(window).resize(function () {
    setsizeFlex();
});
$(window).on('load',function () {
    setsizeFlex();
});
$(document).ready(function () {
    setsizeFlex();

    // widthMenu = ($(window).width() - 45) / 5;
    // $('.menu').css('width', widthMenu);

    // scroll change color
    // $(window).scroll(function () {
    //     // traverse all block and find if we reach or not
    //     $(SELECTOR.BLOCK).each(function () {
    //         let $this = $(this);
    //         let begin = $this.offset().top;
    //         let full_block_height = begin + $this.outerHeight() + parseInt($this.css('padding-bottom'));

    //         // check each menu link only
    //         $(SELECTOR.MENU_HEADER_TEXT).each(function () {
    //             let $link = $(this);
    //             let link_position = $link.offset().top;

    //             if (begin < link_position && full_block_height > link_position) {
    //                 if ($this.hasClass(SELECTOR.BLOCK_DARK)) {
    //                     $link.addClass(SELECTOR.LIGHT_TEXT).removeClass(SELECTOR.DARK_TEXT);
    //                 } else {
    //                     $link.addClass(SELECTOR.DARK_TEXT).removeClass(SELECTOR.LIGHT_TEXT);
    //                 }
    //             }
    //         });
    //     });
    // }).trigger('scroll'); //trigger after event handler to apply the first time.


    // $('.inner').hide(0);

    // $('.outer').mouseenter(function () {
    //     $(this).children('.inner').stop(true, true).slideToggle(800);
    //     $(this).children('.inner').addClass('delay-ani');
    // }).mouseleave(function () {
    //     $(this).children('.inner').slideUp(800);
    //     $(this).children('.inner').removeClass('delay-ani');
    // });

    $(".menu-hamb").click(function () {
        $(".menu-res").addClass("mobile-menu");
        $("body").addClass("height-100vh");
    });
    $(".menu-close").click(function () {
        $(".menu-res").removeClass("mobile-menu");
        $("body").removeClass("height-100vh");
    });

    $(".btn-cookies").click(function () {
        $(".cookies-pop").css("display","none");
    });

    $(".btn-filter").click(function () {
        $(".menu-filter").addClass("mobile-menu");
        $(".menu-res").css("display", "none");
        $("body").addClass("height-100vh");
    });
    $(".menu-close").click(function () {
        $(".menu-filter").removeClass("mobile-menu");
        $(".menu-res").css("display", "flex");
        $("body").removeClass("height-100vh");
    });
    $(".btn-careers").click(function () {
        $(".form-wrap").addClass("display-block");
        $("body").addClass("no-scroll-body")
    });
    $(".form-wrap .close-symbol").click(function () {
        $(".form-wrap").removeClass("display-block");
        $("body").removeClass("no-scroll-body")
    });
    $(".menu-close").click(function () {
        $(".menu-filter").removeClass("mobile-menu");
        $(".menu-res").css("display", "flex");
        $("body").removeClass("height-100vh");
    });

    $('.menu-search').click(function (e) {
        // $('.menu-search span').css("display", "none");
        $(".menu-search input").css("border-bottom", "3px solid");
        $('.menu-footer').css("opacity", "0.3");
        $('.menu-link.h3').css("opacity", "0.3");
        $('.menu-link.logo').css("opacity", "0.3");
        $('.menu-search .arrow-right').css("display", "block");
        $('.menu-link.h3.menu-search').css("opacity", "1");
        $('.line-hover').addClass('no-active-line');
        e.stopPropagation();
    });
    $('body').click(function () {
        // $('.menu-search span').css("display", "block");
        $(".menu-search input").css("border-bottom", "none");
        $('.menu-footer').css("opacity", "1");
        $('.menu-link.h3').css("opacity", "1");
        $('.menu-link.logo').css("opacity", "1");
        $('.menu-search .arrow-right').css("display", "none");
        $('.menu-link.h3.menu-search').css("opacity", "1");
        $('.line-hover').removeClass('no-active-line');
    })
    $(".arrow-down").click(function () {
        var HScroll = $(this).parents('.sec').outerHeight();
        var scrollOffset = $(this).parents('.sec').scrollTop();

        $('html, body').animate({
            scrollTop: scrollOffset + HScroll
        }, 1000);
    });

    AOS.init();
    $('.menu-des').midnight();
    $('.menu-des').css('left', '80%');

    setTimeout(function(){ 
        $('.menu-des .wrap-menu').css('opacity', '1');
    }, 800);

    // check first section to set menu-res color
    // var $firstSec = $('.sec').first();
    // if ( $firstSec.data('midnight') == 'dark-text' ) {
    //     $('.menu').addClass('light-text-init');
    // }
    "dark-text" == $(".sec").first().data("midnight") && $(".menu").addClass("light-text-init");
    $('.menu ~ .careers-location-wrap').addClass("light-text-init");
    // $("body:has(.careers-location-wrap)")
    // "dark-text" == $(".sec.banner-article").first().data("midnight") && (".menu-res .menu-link").removeClass("light-text") && $(".menu-res .menu-link").addClass("dark-text");
    // $(function () {

    //     var $window = $(window);		//Window object

    //     var scrollTime = 1.1;			//Scroll time
    //     var scrollDistance = 300;		//Distance. Use smaller value for shorter scroll and greater value for longer scroll

    //     $window.on("mousewheel DOMMouseScroll", function (event) {

    //         event.preventDefault();

    //         var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
    //         var scrollTop = $window.scrollTop();
    //         var finalScroll = scrollTop - parseInt(delta * scrollDistance);

    //         TweenMax.to($window, scrollTime, {
    //             scrollTo: { y: finalScroll, autoKill: true },
    //             ease: Power1.easeOut,	
    //             autoKill: true,
    //             overwrite: 5
    //         });

    //     });

    // });
    ScrollHandler.init();
})