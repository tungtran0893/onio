let ScrollHandler = {};

const SELECTOR = {
    MENU_FOOTER: ".menu-footer",
    MENU_HEADER: ".menu-item",

    MENU_FOOTER_TEXT: ".menu-text-footer",
    MENU_HEADER_TEXT: ".menu-link",

    // travese block
    BLOCK: "div.sec",
    BLOCK_DARK: "dark",
    BLOCK_LIGHT: "light",

    // text color
    DARK_TEXT: "dark-text",
    LIGHT_TEXT: "light-text",

    // HIDE TEXT
    HIDE_MENU: "hide-menu-scroll"
};

let $menu = null;
let position = "";

ScrollHandler.init = function() {

    position = $(window).scrollTop();
    $menu = $(".menu .menu-des");
    $(window).scroll(function (e) {
        mobile_scroll(e);

        if (typeof turn_off_scroll !== "undefined") {
            return;
        }

        scroll_stop_footer();

        // traverse all block and find if we reach or not
        traverse_block_scroll();
    }).trigger('scroll');
};

let scroll_stop_footer = function() {
    // prepare data
    let menu_end_position = $menu.offset().top + $menu.outerHeight();
    let $last_block = $(SELECTOR.BLOCK + ":last");
    let last_block_position = parseInt($last_block.offset().top);
    let last_block_end_postion = $last_block.outerHeight() + last_block_position;


    // don't scroll after last block full height
    if (last_block_end_postion < menu_end_position) {
        let $body = $("body");
        $body.css('position', 'relative');

        // calculate the bottom percent
        let percent = Math.ceil($(".footer-wrap").outerHeight() / $body.outerHeight() * 100);

        // set
        $menu.removeClass('fixed-important').addClass('absolute-important').css('margin-top',-$menu.outerHeight());
        $menu.parents('.menu').css({'width':'100%', 'height': 0, 'bottom': percent + "%"});
        $menu.parents('.menu').removeClass('fixed-important').addClass('absolute-important');
        return;
    } else {
        // only fixed again if match this
        if ($menu.css('position') === 'absolute' && $(window).scrollTop() >= $menu.offset().top) {
            return;
        }

        $menu.removeClass('absolute-important').addClass('fixed-important').css('margin-top','0');
        $menu.parents('.menu').removeClass('absolute-important').addClass('fixed-important').css('bottom', "");

    }
};

// Traverse block to check scroll
let traverse_block_scroll = function() {
    $(SELECTOR.BLOCK).each(function() {
        let $this = $(this);
        let begin = $this.offset().top;
        let full_block_height = begin + $this.outerHeight() + parseInt($this.css('padding-bottom'));
        //console.log(full_block_height);

        // check each menu link only
        $(SELECTOR.MENU_HEADER_TEXT).each(function () {
            let $link = $(this);
            let link_position = $link.offset().top;

            // check if current menu item reached the block and inside the block
            if (begin < link_position && full_block_height > link_position) {
                if ($this.hasClass(SELECTOR.BLOCK_DARK)) {
                    // $link.addClass(SELECTOR.LIGHT_TEXT).removeClass(SELECTOR.DARK_TEXT);
                } else {
                    // $link.addClass(SELECTOR.DARK_TEXT).removeClass(SELECTOR.LIGHT_TEXT);
                }

                // subscribe hide here
                if ($this.hasClass('sign-up-wrap')) {
                    // hide the menu footer
                    // $menu.find(".menu-footer").addClass('');
                    $menu.find(".menu-footer").css('visibility', 'hidden');
                } else {
                    $menu.find(".menu-footer").css('visibility', 'visible');
                }

                if ($this.hasClass(SELECTOR.HIDE_MENU)) {
                    $link.css('visibility', 'hidden');
                } else {
                    $link.css('visibility', 'visible');
                }
            }
        });
    });
};

function getBodyScrollTop () {
    const el = document.scrollingElement || document.documentElement;
    return el.scrollTop;
}



let mobile_scroll = function(e)
{
    if (window.isOpenMobileMenu) {
        return;
    }

    let scroll = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);

    if (scroll > position) {
        // scroll down
        $menu.parents('.menu').removeClass('fixed').addClass('hide-menu');
    } else {
        // scroll up
        $menu.parents('.menu').addClass('fixed').removeClass('hide-menu');

        if (scroll == 0) {
            $menu.parents('.menu').removeClass('fixed').removeClass('hide-menu');
        }
    }

    position = scroll;
};

// module.exports = ScrollHandler;
